package com.lineate.traineeship;

import java.util.Collection;

public interface Group {
    String getName();

    Collection<User> getUsers();

    void addUser(User user);

    Collection<Permission> getPermissions();
}
